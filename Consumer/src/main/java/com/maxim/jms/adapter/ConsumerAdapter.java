package com.maxim.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;



@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName()); 

	public void sendToMongo(String json) {
		logger.info("Sending to MongoDB");
		MongoClient client = null; 
		try {
			client = new MongoClient();
			MongoDatabase db = client.getDatabase("vendor");
			MongoCollection<Document> collection = db.getCollection("contact");
			logger.info("Converting JSON to DBOject");
			Document object =  Document.parse(json);//(Document) JSON.parse(json);
			collection.insertOne(object);
			logger.info("Sent to MongoDB");
		} finally {
			if (client != null) {
				client.close();
			}
		}

	}
	
}
